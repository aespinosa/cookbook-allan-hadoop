#
# Cookbook Name:: allan-hadoop
# Recipe:: client
#
# Copyright (C) 2014 Allan Espinosa
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

name_node = search(:node, 'recipes:allan-hadoop\:\:name_node').first
node.set['hadoop']['core_site']['fs.defaultFS'] = name_node['fqdn']

resource_manager = search(:node, 'recipes:allan-hadoop\:\:resource_manager').first
node.set['hadoop']['yarn_site']['yarn.resourcemanager.hostname'] = resource_manager['fqdn']

include_recipe 'hadoop'
