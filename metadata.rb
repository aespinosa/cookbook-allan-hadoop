name             'allan-hadoop'
maintainer       'Allan Espinosa'
maintainer_email 'allan.espinosa@outlook.com'
license          'Apache 2.0'
description      'Installs/Configures allan-hadoop'
long_description 'Installs/Configures allan-hadoop'
version          '0.1.0'

depends 'hadoop', '~> 1.1.0'

recipe 'allan-hadoop::client', 'Configures a hadoop client'
