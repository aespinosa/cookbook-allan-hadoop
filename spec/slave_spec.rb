require 'spec_helper'

describe 'allan-hadoop::slave' do
  include SpecHelper

  it 'wraps the community node_manager recipe' do
    expect(chef_run).to include_recipe('hadoop::hadoop_yarn_nodemanager')
  end

  it 'wraps the community data_node recipe' do
    expect(chef_run).to include_recipe('hadoop::hadoop_hdfs_datanode')
  end
end
