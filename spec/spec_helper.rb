require 'chefspec'
require 'chefspec/berkshelf'

module SpecHelper
  def name_node
    { 'fqdn' => 'namenode.example.com' }
  end

  def resource_manager
    { 'fqdn' => 'resourcemanager.example.com' }
  end

  def chef_run
    ChefSpec::Runner.new platform: 'ubuntu', version: '12.04' do |node|
      node.automatic['domain'] = 'example.com'
      stub_command('update-alternatives --display hadoop-conf | grep best | awk \'{print $5}\' | grep /etc/hadoop/conf.chef').and_return(false)
      stub_search(:node, 'recipes:allan-hadoop\:\:name_node').and_return [name_node]
      stub_search(:node, 'recipes:allan-hadoop\:\:resource_manager').and_return [resource_manager]
    end.converge described_recipe
  end
end
