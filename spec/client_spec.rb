require 'spec_helper'

describe 'allan-hadoop::client' do
  include SpecHelper

  it 'wraps the community client recipe' do
    expect(chef_run).to include_recipe('hadoop::default')
  end

  it 'inserts the namenode where the ::name_node recipe is ran' do
    expect(chef_run).to(
      render_file('/etc/hadoop/conf.chef/core-site.xml')
      .with_content(/namenode.example.com/)
    )
  end

  it 'inserts the resourcemanager where the ::resource_manager recipe is ran' do
    expect(chef_run).to(
      render_file('/etc/hadoop/conf.chef/yarn-site.xml')
      .with_content(/resourcemanager.example.com/)
    )
  end
end
