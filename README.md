# allan-hadoop-cookbook

Wrappers to tryout the community hadoop cookbook.

## Supported Platforms

* ubuntu-12.04

## Recipes

* allan-hadoop::name_node
* allan-hadoop::resource_manager
* allan-hadoop::slave

## License and Authors

Author:: Allan Espinosa (<allan.espinosa@outlook.com>)
